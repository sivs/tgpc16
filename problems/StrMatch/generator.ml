open Core.Std

let nBound = 100
let kBound = 10

let rec randStr n =
  if n != 0 then
    let t = if Random.float 1.0 < 0.5 then "R" else "L" in
    t ^ randStr (n-1)
  else
    ""

let revStr s =
  let l = String.length s in
  let rs = ref "" in
  for i = 0 to l-1 do
    rs := ((Char.to_string s.[i]) ^ !rs)
  done;
  !rs

 
(* Must also change rights to left and vice versa *)
let revAndPrint l =
  printf "%s\n" 

let abs a = if a < 0 then -1 * a else a

let rand_pol_str = randStr (abs(Random.int nBound))
let rand_rob_str = randStr (abs(Random.int kBound))

let getValidInstance () =
  let ps = ref rand_pol_str in
  let rs = ref rand_rob_str in
  while not (Str.string_match (Str.regexp !rs) !ps 0) do
    ps := rand_pol_str;
    rs := rand_rob_str;
  done;
  printf "%s %s\n" !ps !rs

let rand_pos_int = Int.abs 
let () =
  getValidInstance ()
