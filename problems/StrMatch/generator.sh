#!/bin/bash
>robberRoute;
>rrRev;
>result
>subroute;
>policeRoute;
>input
>output

polLength=$1
robLength=$2

for i in $(seq 0 $polLength); do
  if (( ($RANDOM % 2) < 1 )); then
    echo -n "Left " >> robberRoute;
  else
    echo -n "Right " >> robberRoute;
  fi
done
echo " " >> robberRoute

startPos=$(( $RANDOM % ($polLength - $robLength )))
pos=0;
printed=0;

while read line; do
  for word in $line; do
    if (($pos < $startPos)); then
      echo -n "$word " >> result;
    
    else #(($pos == $startPos)); then
      echo -n "$word " >> subroute;
      #startPos=$((startPos+1));
      printed=$((printed+1));
    fi
    pos=$((pos+1))
    if (($printed == $robLength)); then
      break;
    fi
  done
  echo $line | awk '{for(i=NF;i>1;i--) printf("%s ",$i); print $1;}' >> rrRev 
done <robberRoute

while read line2; do
  for word in $line2; do
    if [ "$word" == "Left" ];
    then
      #echo "found a left"
      word="Right";
    elif [ "$word" == "Right" ]; 
    then
      #echo "found a right"
      word="Left";
    fi
    echo -n "$word " >> policeRoute
  done
  break
done <rrRev

cat policeRoute >> input
echo " " >> input
cat subroute >> input
