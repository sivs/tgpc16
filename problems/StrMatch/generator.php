#!/usr/bin/php
<?php
$polL = $argv[1];
$robL = $argv[2];
$testID = $argv[3];

$chaseStr = "";

$chaseList = array();
for($i = 0; $i < $polL; $i++)
{
  $word = rand(0,1) == 0 ? "Right" : "Left";
  $chaseStr .= $word; 
  $chaseList[$i] = $word;
  if($i != ($polL - 1))
    $chaseStr .= " ";
}

$robSStart = rand(0, $polL - $robL - 1);

$robStr = "";

for($i = $robSStart; $i < ($robSStart + $robL); $i++) {
  $robStr .= $chaseList[$i];
  if($i != ($robSStart + $robL - 1))
    $robStr .= " ";
}

// Echo the police string
$inputStr = "";

for($i = $polL - 1; $i >= 0; $i--) {
  $inputStr .= $chaseList[$i] == "Right" ? "Left" : "Right";
  if($i != 0)
    $inputStr .= " ";
}

$inputStr .= "\n";
$inputStr .= $robStr;

// Calculate the answer
$robSStart = strpos($chaseStr, $robStr);
if($robSStart == 0)
  echo "Sorry, try again";

$outStr = substr($chaseStr, 0, $robSStart);
file_put_contents("test_$testID.in", $inputStr);
file_put_contents("test_$testID.out", $outStr);
?>
