Your friend Tim accidentally left his copy of God Simulator 2007 
on the Gameboy running 
overnight and approximately 2000 years of in-game time has passed.

Something odd has happened. 
An NPC in the game has realised his world is a simulation,
and is now trying to prove it.

The game has built in security that is currently collapsing the game world
into a singularity, destroying the game state. 
Tim is too invested in this and wants revenge on the NPC for ruining the game.
He plans to challenge the NPC to a battle and force him to surrender.

The Battle Interface is a simplified abstraction of the game
imposed on an 8x8 cell grid. 
Cells can hold 1 or 0 units.
Each unit has very specific rules on movement
but the collapsing singularity is warping spacetime, 
and where those movements will take a unit.

Tim isn't opposed to using cheats. It is his game, after all. 
So he has a single opportunity to swap one unit (except the King) for any other type.
He isn't very forward thinking, and wants immediate results from this cheat.
Help Tim work out the maximum increase of enemy units he can kill on his next turn,
by swapping one of his pieces.

Write a program that takes: 
(NOTE THAT THE BOARD IS NUMBERED WITH THE BOTTOM LEFT CELL BEING (0,0))
1: A 64 line list; each line represents a cell and shows the 8 x and 8 y values representing
the 8 cells this cell is connected to, in the format: 
(cell x, cell y) up-x, up-y, down-x, down-y, left-x, left-y, right-x, right-y,
up-left-x, up-left-y, up-right-x, up-right-y, down-left-x, down-left-y, down-right-x, down-right-y.
If any of these values are -1, then that link is to a location off the board and this move is illegal.

2: And an initial game state showing the (randomly generated) current arrangement of units on the board.

NOTE: example inputs are too big for this page
You can find examples at:
http://lurga.iskrembilen.com/god_sim_examples/

And outputs:
The maximum increase (i.e. the difference between pre-swap and post-swap) 
in opportunities Tim is presented with to kill an enemy unit,
if he swaps any piece (except the King) for any other type.

Note that if an enemy unit is vulnerable to attack by more than one of
Tim's units, this only counts as one opportunity.

Aside from the spacetime being a mess,
the battle interface almost suspiciously resembles chess,
so those familiar pieces will be used.
Enemy pieces do not need to be differentiated,
so they will be represented as an X on the board. 

Unit        (representation on board)
Pawn        (P)
Bishop      (B)
Rook        (R)
Knight      (k)
Queen       (Q)
King        (K)

MOVEMENT RULES:
Pawns (P) can only attack up-left and up-right.

Bishops (B) can attack any enemy 
with an unobstructed diagonal line.

Rooks (R) can attack any enemy 
with an unobstructed vertical or horizontal line.

Knights (k) can move either two cells vertically and one horizontally,
or two cells horizontally and one vertically, and can jump over other pieces.

Queens (Q) can attack with an unobstructed  vertical, 
horizontal, or diagonal line.

Kings (K) can attack adjacent pieces in any direction.
(No castling, and don't worry about putting your own King in check)
