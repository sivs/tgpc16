open Core.Std


let (--) i j = 
    let rec aux n acc =
      if n < i then acc else aux (n-1) (n :: acc)
    in aux j []

type small_move  = C_UP        | C_DOWN 
                 | C_LEFT      | C_RIGHT 
                 | C_UP_LEFT   | C_UP_RIGHT 
                 | C_DOWN_LEFT | C_DOWN_RIGHT

let move_to_str a_move =
  match a_move with
  | C_UP           -> "C_UP"
  | C_DOWN         -> "C_DOWN"
  | C_LEFT         -> "C_LEFT"
  | C_RIGHT        -> "C_RIGHT"
  | C_UP_LEFT      -> "C_UP_LEFT"
  | C_UP_RIGHT     -> "C_UP_RIGHT"
  | C_DOWN_LEFT    -> "C_DOWN_LEFT"
  | C_DOWN_RIGHT   -> "C_DOWN_RIGHT"

let print_move_seq m_seq =
  List.iter ~f:(fun x -> printf " %s" (move_to_str x)) m_seq;
  printf "\n"

let print_move_seq_seq m_seq_seq =
  List.iter ~f:(print_move_seq) m_seq_seq

type t_piece = PAWN | ROOK | KNIGHT | BISHOP | KING | QUEEN | ENEMY | NONE


let rec repeat_move_n_times n move =
  match n with
  | 0 -> []
  | n -> move :: (repeat_move_n_times (n-1) move)

let gen_move_seq move =
  List.map ~f:(fun n -> repeat_move_n_times n move) (1--63)

let int_to_piece i =
  match i with
  | 0 -> PAWN 
  | 1 -> ROOK 
  | 2 -> KNIGHT 
  | 3 -> BISHOP 
  | 4 -> KING 
  | 5 -> QUEEN 
  | 6 -> ENEMY
  | 7 -> NONE


let piece_to_int piece =
  match piece with
  | PAWN    -> 0
  | ROOK    -> 1 
  | KNIGHT  -> 2
  | BISHOP  -> 3
  | KING    -> 4
  | QUEEN   -> 5 
  | ENEMY   -> 6
  | NONE    -> 7

let piece_to_str piece =
  match piece with
  | PAWN    -> "P"
  | ROOK    -> "R" 
  | KNIGHT  -> "k"
  | BISHOP  -> "B"
  | KING    -> "K"
  | QUEEN   -> "Q" 
  | ENEMY   -> "X"
  | NONE    -> " "


let pawn_moves () =
  [[C_UP_LEFT]; [C_UP_RIGHT]]

let king_moves () =
  [[C_UP]; [C_UP_LEFT]; [C_UP_RIGHT]; [C_LEFT]; [C_RIGHT]; [C_DOWN];
  [C_DOWN_LEFT]; [C_DOWN_RIGHT]]

let knight_moves () = [
   [C_UP;C_UP;C_LEFT]; [C_UP;C_UP;C_RIGHT];
   [C_LEFT;C_LEFT;C_UP]; [C_LEFT;C_LEFT;C_DOWN];
   [C_RIGHT;C_RIGHT;C_UP]; [C_RIGHT;C_RIGHT;C_DOWN];
   [C_DOWN;C_DOWN;C_LEFT]; [C_DOWN; C_DOWN; C_RIGHT]
]

let queen_moves () = 
  gen_move_seq C_UP @
  gen_move_seq C_DOWN @
  gen_move_seq C_RIGHT @
  gen_move_seq C_LEFT @
  gen_move_seq C_UP_LEFT @
  gen_move_seq C_UP_RIGHT @
  gen_move_seq C_DOWN_LEFT @
  gen_move_seq C_DOWN_RIGHT

let rook_moves () =
  gen_move_seq C_UP @
  gen_move_seq C_DOWN @
  gen_move_seq C_RIGHT @
  gen_move_seq C_LEFT

let bishop_moves () = 
  gen_move_seq C_UP_LEFT @
  gen_move_seq C_UP_RIGHT @
  gen_move_seq C_DOWN_LEFT @
  gen_move_seq C_DOWN_RIGHT
  
let moves_from_piece piece =
  match piece with
  | PAWN    -> pawn_moves ()
  | ROOK    -> rook_moves ()
  | KNIGHT  -> knight_moves ()
  | BISHOP  -> bishop_moves ()
  | KING    -> king_moves ()
  | QUEEN   -> queen_moves ()
  
type cell =
  {
    this_pos              : int * int;
    mutable c_up          : int * int;
    mutable c_down        : int * int;
    mutable c_left        : int * int;
    mutable c_right       : int * int;
    mutable c_up_left     : int * int;
    mutable c_up_right    : int * int;
    mutable c_down_left   : int * int;
    mutable c_down_right  : int * int
  }

    

let get_id_cell x_pos y_pos = {
  this_pos            = (x_pos, y_pos);
  c_up                = (x_pos, y_pos);
  c_down              = (x_pos, y_pos);
  c_left              = (x_pos, y_pos);
  c_right             = (x_pos, y_pos);
  c_up_left           = (x_pos, y_pos);
  c_up_right          = (x_pos, y_pos);
  c_down_left         = (x_pos, y_pos);
  c_down_right        = (x_pos, y_pos)
}

type t_chess_board =
  {
    g_board : cell array array;
    p_board : t_piece array array
  }


let get_standard_g_board () =
  let default_val = get_id_cell 1 1 in
  let board = Array.make_matrix ~dimx:8 ~dimy:8 default_val in
  for x = 0 to 7 do
    for y = 0 to 7 do
      board.(x).(y) <- get_id_cell x y;
      board.(x).(y).c_up <- (x,y+1);
      board.(x).(y).c_down <- (x, y-1);
      board.(x).(y).c_left <- (x-1, y);
      board.(x).(y).c_right <- (x+1, y);
      board.(x).(y).c_up_left <- (x-1,y+1);
      board.(x).(y).c_up_right <- (x+1,y+1);
      board.(x).(y).c_down_left <- (x-1, y-1);
      board.(x).(y).c_down_right <- (x+1,y-1);
    done;
  done;
  board 

let empty_p_board () = Array.make_matrix 8 8 NONE

let empty_normal_board () =
  let g_board = get_standard_g_board () in
  let p_board = empty_p_board () in
  {g_board;p_board}


let cell_to_str c =
  let (up_x,up_y) = c.c_up in
  let (down_x,down_y) = c.c_down in
  let (left_x, left_y) = c.c_left in
  let (right_x, right_y) = c.c_right in
  let (up_left_x,up_left_y) = c.c_up_left in
  let (up_right_x, up_right_y) = c.c_up_right in
  let (down_left_x,down_left_y) = c.c_down_left in
  let (down_right_x,down_right_y) = c.c_down_right in
  (*printf "up=(%d,%d),down=(%d,%d),left=(%d,%d),right=(%d,%d),up_left(%d,%d),up_right=(%d,%d),down_left=(%d,%d),down_right(%d,%d)" up_x up_y down_x down_y left_x left_y right_x right_y up_left_x up_left_y up_right_x up_right_y down_left_x down_left_y down_right_x down_right_y
*)
  printf "%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d" up_x up_y down_x down_y left_x left_y right_x right_y up_left_x up_left_y up_right_x up_right_y down_left_x down_left_y down_right_x down_right_y
   
let print_g_board board =
  for i = 0 to 7 do
    for j = 0 to 7 do
      let y = 7 - i in
      let x = 7 - j in
      printf "(%d,%d) " x y;
      cell_to_str board.(x).(y);
      printf "\n";
    done;
  done

let is_pos_valid pos =
  let (x,y) = pos in
  (x >= 0) && (x <= 7) && (y >= 0 && y <= 7)

let make_move_safe board pos move =
  let (x,y) = pos in
  let (x2, y2)    = match move with
                  | C_UP -> board.g_board.(x).(y).c_up
                  | C_DOWN -> board.g_board.(x).(y).c_down
                  | C_LEFT -> board.g_board.(x).(y).c_left
                  | C_RIGHT  -> board.g_board.(x).(y).c_right
                  | C_UP_LEFT -> board.g_board.(x).(y).c_up_left
                  | C_UP_RIGHT -> board.g_board.(x).(y).c_up_right
                  | C_DOWN_LEFT -> board.g_board.(x).(y).c_down_left
                  | C_DOWN_RIGHT -> board.g_board.(x).(y).c_down_right in
  
  if (is_pos_valid (x2,y2)) then Some (x2,y2) else None

let print_pos pos =
  let (x,y) = pos in
  printf "(%d,%d)" x y

let print_piece_board piece_board =
 for i = 0 to 7 do
   let y = 7 - i in
   printf "%d" (y);
  for x = 0 to 7 do
    printf " %s" (piece_to_str piece_board.(x).(y));
  done;
  printf "\n";
 done;
 printf " ";
 for x = 0 to 7 do
  printf " %d" (x);
 done

let rec make_moves board in_pos moves piece =
  match moves with
  | move :: next_move :: other_moves -> 
    begin
      match make_move_safe board in_pos move with
      | Some (next_x, next_y) -> 
          if board.p_board.(next_x).(next_y) = NONE || piece = KNIGHT
          then  
            make_moves board (next_x, next_y) (next_move::other_moves) piece
          else
            (*let something = print_piece_board board.p_board; printf "Can't jump over this piece %d at " (piece_to_int (board.p_board.(next_x).(next_y))); print_pos (next_x, next_y);
            printf "\n"; in*)
            None (* Can't jump over this piece *)
      | None -> None
    end
  | move :: [] ->
      begin
        match make_move_safe board in_pos move with
        | Some (next_x, next_y) ->
          if (board.p_board.(next_x).(next_y) = NONE || board.p_board.(next_x).(next_y) = ENEMY) then
            Some (next_x, next_y)
          else
            None (* You can't place it on top of one of your own pieces *)
        | None -> None
      end
  (*| [] -> Some in_pos *)


(* Find to random cells and swap them *)
let mess_up g_board =
  let (x1,y1) = (Random.int 7, Random.int 7) in
  let new_pos = (Random.int 7, Random.int 7) in
  match (Random.int 7) with
  | 0 -> g_board.(x1).(y1).c_up <- new_pos
  | 1 -> g_board.(x1).(y1).c_down <- new_pos
  | 2 -> g_board.(x1).(y1).c_right<- new_pos
  | 3 -> g_board.(x1).(y1).c_left<- new_pos
  | 4 -> g_board.(x1).(y1).c_up_left<- new_pos
  | 5 -> g_board.(x1).(y1).c_up_right<- new_pos
  | 6 -> g_board.(x1).(y1).c_down_left<- new_pos
  | 7 -> g_board.(x1).(y1).c_down_right<- new_pos


let rand_own_piece () = int_to_piece (Random.int 5)

let add_own piece_board =
  let (x,y) = (Random.int 7, Random.int 7) in
  piece_board.(x).(y) <- rand_own_piece () 

let add_enemy piece_board =
  let (x,y) = (Random.int 7, Random.int 7) in
  piece_board.(x).(y) <- ENEMY



let print_pos_list p_list =
  List.iter ~f:(print_pos) p_list;
  printf "\n"

let can_reach board (a : (int * int)) (t : (int * int)) (piece:t_piece) =
  let g_board = board.g_board in
  let p_board = board.p_board in
  let new_pos_list = ( List.map ~f:(fun x -> make_moves board a x piece) ((moves_from_piece piece) : small_move list list)) in
  let valid_pos_list = List.filter ~f:(fun x -> match x with Some _ -> true | None -> false) new_pos_list in
  let pos_list      = List.map ~f:(fun (Some x) -> x) valid_pos_list in
  let res = List.mem pos_list t in
  res

let can_reach_dbg board (a : (int * int)) (t : (int * int)) (piece:t_piece) =
  let g_board = board.g_board in
  let p_board = board.p_board in
  let new_pos_list = ( List.map ~f:(fun x -> make_moves board a x piece) ((moves_from_piece piece) : small_move list list)) in
  let valid_pos_list = List.filter ~f:(fun x -> match x with Some _ -> true | None -> false) new_pos_list in
  let pos_list      = List.map ~f:(fun (Some x) -> x) valid_pos_list in
  print_pos_list pos_list

let fucking_head l =
  match List.hd l with
  | Some l -> l
  | None -> []

let all_friends board =
  let enemy_arr = Array.create 100 (-1,-1) in
  let i = ref (-1) in
  for y = 0 to 7 do
    for x = 0 to 7 do
      let piece_code = piece_to_int board.p_board.(x).(y) in
      if piece_code >= 0 && piece_code < 6 then
      begin
        i := !i + 1;
        enemy_arr.(!i) <- (x,y)
      end
    done;
  done;
  List.filter ~f:(fun x -> x != (-1,-1)) (Array.to_list enemy_arr)

let all_enemies board =
  let enemy_arr = Array.create 100 (-1,-1) in
  let i = ref (-1) in
  for y = 0 to 7 do
    for x = 0 to 7 do
      if board.p_board.(x).(y) = ENEMY then
      begin
        i := !i + 1;
        enemy_arr.(!i) <- (x,y)
      end
    done;
  done;
  List.filter ~f:(fun x -> x != (-1,-1)) (Array.to_list enemy_arr)

let can_kill_dbg pos board =
  let friends = all_friends board in
  let willing_to_kill = List.filter ~f:(fun (x,y) -> can_reach board (x,y) pos board.p_board.(x).(y)) friends in
  match willing_to_kill with
  | hd :: tl ->
      printf "Yes\n";
      print_pos (hd)
  | _ ->
      printf "No\n"

let can_kill pos board =
  let friends = all_friends board in
  List.exists ~f:(fun (x,y) -> can_reach board (x,y) pos board.p_board.(x).(y)) friends

let get_score board =
  List.length ((List.filter ~f:(fun x -> can_kill x board) (all_enemies board)))

let max_int a b =
  if a > b then a else b

let get_max board =
  let friends = all_friends board in
  let friends_arr = Array.of_list friends in
  let max_score = ref (get_score board) in
  let best_pos = ref (-1,-1) in
  let best_piece = ref NONE in
  for i = 0 to (Array.length friends_arr - 1) do
    let (x,y) = friends_arr.(i) in
    let friend_type = board.p_board.(x).(y) in
    if friend_type != KING then
      begin
      board.p_board.(x).(y) <- QUEEN;
      max_score := max_int (!max_score) (get_score board);
      board.p_board.(x).(y) <- KNIGHT;
      max_score := max_int (!max_score) (get_score board);
      board.p_board.(x).(y) <- friend_type
      end;
  done;
  !max_score


let () =
  let n_own = int_of_string Sys.argv.(1) in
  let n_enemies = int_of_string Sys.argv.(2) in
  let fucked_uppiness = int_of_string Sys.argv.(3) in
  let board = empty_normal_board () in
  for i = 0 to n_own do
    add_own board.p_board;
  done;
  for i = 0 to n_enemies do
    add_enemy board.p_board;
  done;
  for i = 0 to fucked_uppiness do
    mess_up board.g_board;
  done;
  print_g_board board.g_board;
  print_piece_board board.p_board;
  let first_score = get_score board in
  let second_score = get_max board in
  printf "\n";
  printf "Ans: %d" (second_score - first_score)
