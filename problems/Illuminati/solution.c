#include <stdio.h>
#include <math.h>
#include <stdlib.h>


int main()
{
  double max = 0.0;

  double x1, x2, x3, y1, y2, y3, dx;
  while(7==scanf("(%lf, %lf, %lf) (%lf, %lf, %lf) %lf",&x1,&x2,&x3,&y1,&y2,&y3,&dx))
  {
    double d2 = sqrt(pow(x1-y1, 2.0) + pow(x2-y2, 2.0) + pow(x3-y3,2.0));
    double diff = abs(d2 - dx);
    if(diff > max) {
      max = diff;
    }
  }

  printf("%s\n", max != 0.0 ? "Yes" : "No");
  return 0;
}
