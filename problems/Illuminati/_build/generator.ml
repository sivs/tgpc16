open Core.Std

let bound   = 100
let dBound  = 1000.0
let nBound  = 10


let dist (x1, x2, x3) (y1, y2, y3) =
    sqrt ((x1 -. y1)**2.0 +. (x2 -. y2)**2.0 +. (x3 -.
    y3)**2.0)

let rDist () =
  Random.float dBound
 
let () =
  if Array.length Sys.argv != 5
  then
    printf "./generator.native bound_on_coordinates num_points dist_bound
    is_the_theory_true(1 || 0)\n"
  else
    let bound = float_of_string Sys.argv.(1) in
    let nBound = int_of_string Sys.argv.(2) in
    let dbound = float_of_string Sys.argv.(3) in
    let conspiracyTrue = (int_of_string Sys.argv.(4)) = 1 in
    for n = 0 to nBound do
      let x1    = Random.float bound in
      let x2    = Random.float bound in
      let x3    = Random.float bound in
      let y1    = Random.float bound in
      let y2    = Random.float bound in
      let y3    = Random.float bound in
      let dist  = if conspiracyTrue 
                  then 
                    Random.float dBound 
                  else 
                    dist (x1,x2,x3) (y1,y2,y3) in
      printf "(%.4f, %.4f, %.4f) (%.4f, %.4f, %.4f) %.4f\n" x1 x2 x3 y1 y2 y3 dist
    done
