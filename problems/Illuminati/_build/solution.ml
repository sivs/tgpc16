open Core.Std

let getInput () = 
let lines = ref [] in
let get_line = In_channel.input_line stdin in
try
  while true; do
    let the_line = match get_line with | Some s -> s | None -> "" in
    assert(the_line!="");
    lines := the_line :: !lines
  done; !lines
with End_of_file ->
  List.rev !lines


let from_string s =
  let f n x1 x2 x3 y1 y2 y3 d =
    (n, abs_float (d -. sqrt ((x1 -. y1)**2.0 +. (x2 -. y2)**2.0 +. (x3 -. y3)**2.0))) in
  let (n, e) = Scanf.sscanf s "%d (%f, %f, %f) (%f, %f, %f) %f" f in
  (n, e)
  (*printf "%d, %f\n" n e*)
 
let () =
  try
    let lines = getInput () in
    let (n,e) = List.fold_left ~f:(fun (n1, e1) (n2, e2) -> if e1 > e2 then (n1,
    e1) else (n2, e2)) ~init:(-1, 0.0) (List.map ~f:from_string lines) in
    printf "i: %d, e: %.4f\n" n e
  with
    End_of_file -> printf "Fuckingshit.jpg\n"
  (*List.iter ~f:from_string lines*)
